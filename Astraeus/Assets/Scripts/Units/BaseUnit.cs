using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Astraeus.Characters
{
    public class BaseUnit : MonoBehaviour
    {
        [SerializeField] UnitStats stats;

        public UnitStats Stats
		{
            get { return stats; }
            set { stats = value; }
		}
    }
}
