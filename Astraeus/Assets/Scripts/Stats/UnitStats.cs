using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewUnitStats", menuName = "Units/Create New Unit Stats")]
public class UnitStats : ScriptableObject
{
	public string unitName;

	public int moveRange;
}

