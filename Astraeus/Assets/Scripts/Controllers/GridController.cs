using System.Collections;
using System.Collections.Generic;
using Astraeus.Characters;
using UnityEngine;

namespace Astraeus.Movement
{
    public class GridController : MonoBehaviour
    {
        Dictionary<Vector3Int, GridNode> grid = new Dictionary<Vector3Int, GridNode>();
        Queue<GridNode> nodeQueue = new Queue<GridNode>();

        Vector3Int[] directions =
        {
        Vector3Int.forward,
        Vector3Int.right,
        Vector3Int.back,
        Vector3Int.left
        };

        public void CreateGrid()
		{
            //find all of the Nodes
            GridNode[] nodes = GetComponentsInChildren<GridNode>();

            //add their position to the dictionary
            foreach (GridNode node in nodes)
            {
                var gridPos = node.AttachToGrid();

                //if there are two Nodes at the same coordinates, skip the second one
                if (grid.ContainsKey(gridPos))
                {
                    Debug.Log("Skipping overlapping block : " + node);
                }
                else
                {
                    //add to grid
                    grid.Add(gridPos, node);
                }
            } 
        }

        public void FindMoveNodes(MovementComponent currentUnit)
		{
            GridNode startingNode = grid[currentUnit.GridPos];

            //create path list
            nodeQueue = new Queue<GridNode>();

            BreadthFirstSearch(startingNode, currentUnit);

            //store start node for undo move purposes
            currentUnit.PreviousNode = startingNode;
        }

        private void BreadthFirstSearch(GridNode startingNode, MovementComponent currentUnit)
		{
            //add the starting Node to the queue
            nodeQueue.Enqueue(startingNode);

            //while there are Nodes still to check and the end hasnt been found
            while (nodeQueue.Count > 0)
            {
                //remove current Node from queue
                GridNode currentNode = nodeQueue.Dequeue();

                //Add neighbours of the current Node to the queue
                CheckNeighbours(currentNode, currentUnit);

                //set the current waypooint as explored
                currentNode.IsExplored = true;
            }
        }

        private void CheckNeighbours(GridNode currentNode, MovementComponent currentUnit)
		{
            //search for a neighbour in all 4 directions
            foreach (Vector3Int direction in directions)
            {
                //find the current neighbour to be checked
                Vector3Int neighbourCoordinates = currentNode.GridPos + direction;

                //if the neighbour Node exists
                if (grid.ContainsKey(neighbourCoordinates))
                {
                  //add it to the queue
                  QueueNewNeighbours(neighbourCoordinates, currentNode, currentUnit);
                }
            }
        }

        private void QueueNewNeighbours(Vector3Int neighbourCoordinates, GridNode currentNode, MovementComponent currentUnit)
		{
            //get the neighbour Node at the coordinates
            GridNode neighbour = grid[neighbourCoordinates];

            //if the neighbour hasnt already been searched and isnt already in the queue
            if (!neighbour.IsExplored || nodeQueue.Contains(neighbour))
            {
                //if the neighbour is passable and within the unit's move range
                if ((currentNode.TotalMovementCost + neighbour.MovementCost) <= currentUnit.GetComponent<BaseUnit>().Stats.moveRange)
                {
                    //if the node isnt occupied, occupied by a knocked out unit or occupied by a teammate
                    if (!neighbour.Occupant)
                    {
                        //add it to the queue and set where it was added from
                        nodeQueue.Enqueue(neighbour);
                        neighbour.ExploredFrom = currentNode;

                        //give the node a movement cost
                        neighbour.TotalMovementCost = currentNode.TotalMovementCost + neighbour.MovementCost;
                        neighbour.IsMoveNode = true;
                    }
                }
            }
        }
    }
}
