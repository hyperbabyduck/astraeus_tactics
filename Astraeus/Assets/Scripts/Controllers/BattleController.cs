using System.Collections;
using System.Collections.Generic;
using Astraeus.Movement;
using UnityEngine;

public struct Global
{
	public const int GRID_SIZE = 1;
	public const float ELEVATION_STEP = 0.25f;
}

namespace Astraeus.Battle
{
	public class BattleController : MonoBehaviour
	{
		GridController gridController;

		private void Start()
		{
			gridController = FindObjectOfType<GridController>();
			gridController.CreateGrid();
		}
	}
}
