using System;
using System.Collections;
using System.Collections.Generic;
using Astraeus.Movement;
using UnityEngine;

public enum UserInput
{
    None,
    Move
}

namespace Astraeus.Battle
{
    public class PlayerController : MonoBehaviour
    {
        UserInput inputState = UserInput.None;
        GridNode selectedNode;

		private void Start()
		{
            InputState = UserInput.Move;
		}

		private void Update()
        {
            if (InputState != UserInput.None)
            {
                InteractWithNode();
            }
        }

        public UserInput InputState
		{
            get { return inputState; }
			set { inputState = value; }
		}

        private bool InteractWithNode()
        {
            //get all raycast hits and sort them into a list
            RaycastHit[] hits = RaycastAllSorted();

            //go thorough each of the hits
            foreach (RaycastHit hit in hits)
            {
                //get the raycastable component from the object hit
                IRaycastable raycastableComponent = hit.transform.GetComponent<IRaycastable>();

                //if the object is hit by the raycast and active
                if (raycastableComponent.HandleRaycast())
                {
                    GridNode hitNode = hit.transform.GetComponent<GridNode>();

                    //if component has a node component
                    if (hitNode)
                    {
                        if (hitNode != selectedNode)
                        {
                            if (selectedNode)
                            {
                                //wipe old node
                                selectedNode.HighlightNode(false);
                            }

                            //highlight new node
                            selectedNode = hitNode;
                            selectedNode.HighlightNode(true);
                        }

                        return true;
                    }
                    else
                    {
                        if (selectedNode)
                        {
                            //wipe old node
                            selectedNode.HighlightNode(false);
                            selectedNode = null;
                        }
                    }
                }
            }

            //wipe node if any is selected and no node is highlighted
            if (selectedNode)
            {
                //wipe old node
                selectedNode.HighlightNode(false);
                selectedNode = null;
            }
            return false;
        }

        private RaycastHit[] RaycastAllSorted()
        {
            RaycastHit[] hits = Physics.RaycastAll(GetMouseRay());
            float[] distances = new float[hits.Length];

            for (int index = 0; index < hits.Length; index += 1)
            {
                distances[index] = hits[index].distance;
            }

            Array.Sort(distances, hits);

            return hits;
        }

        private Ray GetMouseRay()
        {
            return Camera.main.ScreenPointToRay(Input.mousePosition);
        }
    }
}
