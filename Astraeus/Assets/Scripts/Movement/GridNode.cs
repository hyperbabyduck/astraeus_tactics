using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Astraeus.Characters;
using UnityEngine;

namespace Astraeus.Movement
{
    public class GridNode : MonoBehaviour, IRaycastable
    {
        [SerializeField] SpriteRenderer nodeOutline;
        [SerializeField] SpriteRenderer nodeFill;

        Vector3Int gridPos;
        bool isExplored = false;
        bool isMoveNode;
        GridNode exploredFrom = default;
        BaseUnit occupant = null;
        float movementCost;
        float totalMovementCost;

        public Vector3Int AttachToGrid()
		{
            gridPos = new Vector3Int(
            Mathf.RoundToInt(transform.position.x / Global.GRID_SIZE),
            Mathf.RoundToInt(transform.position.y / Global.ELEVATION_STEP),
            Mathf.RoundToInt(transform.position.z / Global.GRID_SIZE)
            );

            SetCoordinateText();
            return gridPos;
        }

        public void SetCoordinateText()
		{
            Text text = GetComponentInChildren<Text>();
            text.text = ("x: " + gridPos.x + "\ny: " + gridPos.z + "\nh: " + gridPos.y.ToString("0.00"));
		}


        public void HighlightNode(bool isHighlighted)
		{
            if (isHighlighted)
            {
                nodeFill.color = Color.cyan;
                nodeFill.enabled = true;
            }
            else
			{
                nodeFill.enabled = false;
                nodeFill.color = Color.white; 
            }
		}

		//GETTER/SETTERS
		public Vector3Int GridPos
		{
            get { return gridPos; }
            set { gridPos = value; }
		}

        public bool IsExplored
		{
            get { return isExplored; }
            set { isExplored = value; }
		}

        public bool IsMoveNode
		{
            get { return isMoveNode; }
            set { isMoveNode = value; }
        }

        public GridNode ExploredFrom
        {
            get { return exploredFrom; }
            set { exploredFrom = value; }
        }

        public BaseUnit Occupant
        {
            get { return occupant; }
            set { occupant = value; }
        }

        public float MovementCost
        {
            get { return movementCost; }
            set { movementCost = value; }
        }

        public float TotalMovementCost
        {
            get { return totalMovementCost; }
            set { totalMovementCost = value; }
        }

        public bool HandleRaycast()
        {
            return true;
        }
    }
}
