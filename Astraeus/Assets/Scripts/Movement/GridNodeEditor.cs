using System.Collections;
using System.Collections.Generic;
using Astraeus.Movement;
using UnityEngine;

using UnityEngine.UI;

[ExecuteInEditMode]
[SelectionBase]
[RequireComponent(typeof(GridNode))]

public class GridNodeEditor : MonoBehaviour
{
    GridNode node;

    private void Awake()
    {
        node = GetComponent<GridNode>();
    }

    // Update is called once per frame
    void Update()
    {
        node.AttachToGrid();
        node.SetCoordinateText();
    }
}