using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Astraeus.Movement
{
    public class MovementComponent : MonoBehaviour
    {
        Vector3Int gridPos;
        GridNode previousNode;

        public Vector3Int AttachToGrid()
        {
            gridPos = new Vector3Int(
            Mathf.RoundToInt(transform.position.x / Global.GRID_SIZE),
            Mathf.RoundToInt(transform.position.y / Global.ELEVATION_STEP),
            Mathf.RoundToInt(transform.position.z / Global.GRID_SIZE)
            );

            return gridPos;
        }

        //GETTER/SETTERS
        public Vector3Int GridPos
        {
            get { return gridPos; }
            set { gridPos = value; }
        }

        public GridNode PreviousNode
		{
            get { return previousNode; }
            set { previousNode = value; }
		}
    }
}
